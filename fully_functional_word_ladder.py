import re
import os
#checks to see if item and target words are the same length.
def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

#Returns a word if it is found in the pattern, and it hasn't been seen before.
def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]

#Finds the target in relation to the starting word, and returns
def find(word, words, seen, target, path):
  list = []
  for i in range(len(word)):
    #Appends the words returned from build
    if word[i] != target[i]:
      list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  if len(list) == 0:
    return False
  #
  list = sorted([(same(w, target), w) for w in list], reverse = True)

  for (match, item) in list:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
  for (match, item) in list:
    path.append(item)
    if find(item, words, seen, target, path):
      return True
    path.pop()

fname = input("Enter dictionary name: ")
#error handling: filename exceptions.
if os.path.exists(fname) == False:
  print("File missing; must be a valid text file.")
  wait = input("PRESS ENTER TO CONTINUE.")
  exit()
if len(fname) > 64:
  print("Invalid filename; filename length too long.")
  wait = input("PRESS ENTER TO CONTINUE.")
  exit()

#banned word dictionary input and error handling
banned = input("Enter dictionary of banned words if wanted:, else, leave blank:")
if os.path.exists(banned) == False and banned != "":
  print("File missing; must be a valid text file.")
  wait = input("PRESS ENTER TO CONTINUE.")
  exit()
if len(fname) > 64:
  print("Invalid filename; filename length too long.")
  wait = input("PRESS ENTER TO CONTINUE.")
  exit()

#reads the banned words dictionary and creates the banned word list
if banned != "":
  file = open(banned)
  lines = file.readlines()
  while True:
    bannedWords = []
    for line in lines:
      word = line.rstrip()
      bannedWords.append(word)
    break




#error handling: Checking if file is populated.
"""if len(file.split()) == 0:
  print("Dictionary is empty! Please enter a valid dictionary.")
  wait = input("PRESS ENTER TO CONTINUE.")
  exit()"""



file = open(fname)
lines = file.readlines()
while True:
  start = input("Enter start word:")

  # error handling: words must contain letters only.
  if str.isalpha(start) == False:
    print("Invalid word, try again!")
    wait = input("PRESS ENTER TO CONTINUE.")
    exit()

#creates words list
  words = []
  for line in lines:
    word = line.rstrip()
    if len(word) == len(start):
      words.append(word)

#checks banned words against dictionary words
  if banned != "":
    for bannedWord in bannedWords:
     for word in words:
      if word == bannedWord:
        words.remove(word)


#Section for handling a bridge word
  bridge = input("If you want a bridging word, enter it now, else, leave blank:")
  if str.isalpha(bridge) == False and bridge != "":
    print("Invalid word, try again!")
    wait = input("PRESS ENTER TO CONTINUE.")
    exit()
  if len(bridge) != len (start) and bridge != "":
    print("Words must be the same size!")
    wait = input("PRESS ENTER TO CONTINUE.")
    exit()

  #Declaring path for later use (work around for bridge implementation).
  path = [start]

#If bridge is entered, start a path as bridge as the target
  if bridge != "":
    target = bridge
    count = 0
    path = [start]
    seen = {start: True}
    if find(start, words, seen, target, path):
      path.append(target)

    else:
      print("No path found")
      wait = input("PRESS ENTER TO CONTINUE.")
      exit()
    #Bridge is now start word
    start = bridge

  target = input("Enter target word:")

  # error handling: words must contain letters only.
  if str.isalpha(target) == 'false':
    print("Invalid word, try again!")
    wait = input("PRESS ENTER TO CONTINUE.")
    exit()
  # error handling: words must be same size.
  if len(start) != len(target):
    print("Start word and target word must be same length!")
    wait = input("PRESS ENTER TO CONTINUE.")
    exit()
  break

count = 0
if path == None:
  path = [start]

seen = {start : True}
if find(start, words, seen, target, path):
  path.append(target)
  print(len(path) - 1, path)
else:
  print("No path found. Try some different words!")
  wait = input("PRESS ENTER TO END.")
